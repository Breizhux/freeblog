#!/usr/bin/env python3
# -*- coding : utf-8 -*-


#Nombre d'articles à afficher dans les listes d'article
POSTS_NUMBER = 10

#Nombre d'articles à intégrer dans le flux rss
FEED_POSTS_NUMBER = 20

#Pour une recherche, si le score de correspondance est supérieur à cette limite,
#on rajoute au résultat, même si on doit dépasser le nombre de posts (POSTS_NUMBER)
POST_SCORE_LIMIT = 2000


#OPTION COMMUNE = présent dans toute les pages
COMMON_OPTIONS = {
    #Nom du blog à afficher dans le bandeau supérieur
    "blog_name" : "MySimpleFlaskBlogName",

    #Page d'accueil (obligatoire)
    #"home" : "/home.html", → your home need to be templates/home.html

    #Liste des catégories possibles dans le bandeau supérieur
    "categories" : {
        #nom de la catégories : nom à afficher
        #doit correpondra à un dossier/fichier : posts/<categorie>/<categorie>.html
        "actualites" : "Actualités",    # correspond à : posts/actualites/actualites.html
        "tutoriels" : "Tutoriels",      #etc.
        "cours" : "Cours",
        "rechercher" : "Rechercher",
        "contacts" : "Contacts",
    },

    #Liste des moyens pour suivre les activités du site (réseau sociaux, etc...)
    #   ("chemin de l'icône correspondante", "url cible") → les images doivent se trouver dans le dossier static/feeds
    "feeds" : [
        ("rss_feed.png", "/feed"),
    ],
    "feed_sentence" : "Suivez nous sur ces sites :",

    #OPTIONS DE LA BARRE DE RECHERCHE, ET ENREGISTREMENT DES DERNIÈRES OPTIONS
    "input_sentence" : "Rechercher un article",
    "sort_option" : {
        'sentence' : "Trier par",   #phrase précédent la liste
        'relevance' : "Pertinence", #défaut
        'date' : "Date",
        'alphanum' : "Alphabétique"
    },
}

#OPTIONS POUR LES ARTICLES
POSTS_OPTIONS = {
    #Format des dates dans les métadonnées du fichier markdown
    "date_format" : "%d/%m/%Y",

    #Phrase des métadonnées avec clé : author (auteur), date
    "metadata_sentence" : "Auteur : {author}, Date : {date}",

    #Si le logo n'est pas donné dans les métadonnées mais que le fichier suivant existe à côté, il sera utilisé.
    "default_logo_name" : "illustration.png",
}

#INFORMATIONS POUR LE FLUX RSS
FEED_INFOS = {
    #Titre du flux rss
    "title" : "FlusRSS pour MySimpleFlaskBlog",
    #Lien d'accès depuis le flux rss
    "link" : "http://127.0.0.1:8080/",
    #Description du flux RSS
    "description" : "Ceci est le flux rss de mon SimpleFlaskBlog.",
    #langue principale du flux
    "language" : 'fr',
}

#Description par défaut d'un article
#ATTENTION :
#Selon la date configuré et si le markdown n'intègre pas de date, le document peut finir en fin de liste.
#Si aucune description n'est donnée, les recherches risques de ne jamais donner le document.
DEFAULT_METADATA = {
    "title" : None,
    "author" : "anonyme",
    "date" : "01/01/1970",
    "description" : "Pas de description",
    "keywords" : [],
    "logo" : "/static/images/default_illustration.png", #ne pas toucher celui-ci !
}
