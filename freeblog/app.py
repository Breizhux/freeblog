#!/usr/bin/env python3
# -*- coding : utf-8 -*-

from conf import *
from blog_tools import *
from flaskext.markdown import Markdown
from flask import Flask, render_template, redirect, url_for, send_file, request, make_response

DEBUG = True



app = Flask(__name__)
md_engine = Markdown(
    app,
    extensions=['markdown.extensions.tables', 'markdown.extensions.md_in_html',
                'markdown.extensions.attr_list', 'markdown.extensions.def_list',
                'markdown.extensions.abbr',
                'pymdownx.tasklist', 'pymdownx.arithmatex', 'pymdownx.highlight',
                'pymdownx.inlinehilite', 'pymdownx.keys', 'pymdownx.superfences',
                'pymdownx.progressbar', 'pymdownx.smartsymbols', 'pymdownx.mark',
                'pymdownx.tilde', 'pymdownx.tabbed',
                'legacy_attrs', 'mdx_truly_sane_lists', 'smarty', 'mdx_unimoji',
                'sane_lists', 'nl2br', 'mdx_superscript', 'codehilite',
               ],
    safe_mode=True,
    output_format='html5',
)



@app.route('/')
@app.route('/home.html')
def home() :
    print("\n[accueil] redirect to home")
    return render_template('/home.html',
                           posts=posts_list_to_html(get_latest_posts('/home.html')),
                           title=COMMON_OPTIONS['blog_name'],
                           **COMMON_OPTIONS)


@app.route('/posts/<path:path>', methods=['GET'])
def categories(path) :
    #path : <categories>/<categories>.html
    #path : <categories>/<article_name>/<filename.ext_sauf_md>

    #Cas des pièces jointes
    if not ".html" in path :
        print("\n[attachment] \"{}\"".format(path))
        return send_file("templates/posts/"+path)

    #Cas d'une recherche
    elif "search" in request.args and len(request.args.get("search")) != 0 :
        print("\n[search] {}".format(request.args.get("search")))
        match_posts = get_search_posts(path, request.args.get("search"))
        return render_template("/posts/"+path,
                               posts=posts_list_to_html(match_posts, sort=request.args.get("sort_order")),
                               search_value=request.args.get("search"),
                               sort_value=request.args.get("sort_order"),
                               title=get_main_title(path),
                               search_bar=True,
                               **COMMON_OPTIONS)

    #Cas de l'acceuil d'une catégorie
    elif ".html" in path :
        print("\n[categories] redirect to {}".format(path[path.rfind('/')+1:]))
        return render_template("/posts/"+path,
                               posts=posts_list_to_html(get_latest_posts(path)),
                               sort_value="relevance",
                               title=get_main_title(path),
                               search_bar=True,
                               **COMMON_OPTIONS)


@app.route('/posts/<path:path>/<article_name>.md')
def article(path, article_name) :
    path = "posts/{}/{}.md".format(path, article_name)
    print("\n[posts] Try to read \"{}\"".format(path))
    md_file = MdFile("templates/{}".format(path))
    return render_template("md_rendered.html", markdown_article=md_file.get_text(),
                           search_bar=False,
                           title=get_main_title(md_file),
                           **COMMON_OPTIONS)



@app.route('/feed', methods=['GET'])
def feed() :
    print("[rss] want access to the rss feed")
    #set the categorie user want, default, return all categories
    categorie = "/feed"
    if "categorie" in request.args :
        categorie = "{}/".format(request.args.get("categorie"))
        if not categorie in COMMON_OPTIONS['categories'] :
            categorie = "/feed"
    #return rss result
    rss_feed = get_rss_feed(categorie)
    response = make_response(rss_feed)
    response.headers.set('Content-Type', 'application/rss+xml')
    return response



if __name__ == '__main__' :
    host = "127.0.0.1"
    port = 8080
    app.run(debug=DEBUG, host=host, port=port)
