#!/usr/bin/env python3
# -*- coding : utf-8 -*-

import os
import pathlib
from conf import *
from rfeed import *
from md_manager import MdFile
from fuzzywuzzy import fuzz, process



HTML_POST_DESCRIPTION = """
        <div class="post">
            <a href="$LINK" class="post_title">$TITLE</a>
            <div class="box_in_line">
                    <img class="post_illustration" src="$LOGO" decoding="async"/>
                <div>
                    <p class="post_description">$DESCRIPTION</p>
                    <p class="post_metadata">$METADATA</p>
                </div>
            </div>
        </div>
"""



def iter_posts(source) :
    """ Iterate the path of different posts from
    the categories call this function."""
    #source : <categories>/<categories>.html
    if source == "/home.html" or source == "/feed" :
        paths = COMMON_OPTIONS['categories'].keys()
    else :
        path = os.path.dirname(source)
        if os.path.exists("templates/posts/{}/search_is_global".format(path)) :
            paths = COMMON_OPTIONS['categories'].keys()
        else :
            paths = [path]
    for categorie in paths :
        categ_path = "templates/posts/{}".format(categorie)
        for post in os.listdir(categ_path) :
            if post == categorie+".html" : continue
            if post == "search_is_global" : continue
            post_path = "{}/{}/{}.md".format(categ_path, post, post)
            yield post_path



def get_latest_posts(path, post_number=POSTS_NUMBER) :
    """ Return the latests posts for the categories call this function."""
    posts = [] #(None, date, title, md_file)
    for post in iter_posts(path) :
        md_file = MdFile(post)
        date = md_file.get_date(unixts=True)
        #Si on a pas le nombre d'article minimum, on l'ajoute
        if len(posts) < post_number :
            posts.append((None, date, md_file['title'], md_file))
            continue
        #Si l'article le moins récent gardé est plus vieux que l'actuel, on l'ajoute
        oldest_posts = min(posts, key=lambda k: k[1])
        if date > oldest_posts[1] :
            posts.remove(oldest_posts)
            posts.append((None, date, md_file['title'], md_file))
    return posts

def get_search_posts(path, text) :
    """ Return the best match articles."""
    #list best match
    match_posts = [] #(score, date, titre, md_file)
    for post in iter_posts(path) :
        md_file = MdFile(post)
        score = fuzz.token_sort_ratio(text, md_file['description'])
        score *= 4*fuzz.partial_token_sort_ratio(text, md_file['title'])
        date = md_file.get_date(unixts=True)
        #Si on a pas atteint le nombre de posts
        if len(match_posts) < POSTS_NUMBER :
            match_posts.append((score, date, md_file['title'], md_file))
            continue
        lower_match = min(match_posts, key=lambda k: k[0])
        #Si le score est supérieur au plus petit des scores
        if score > lower_match[0] :
            match_posts.remove(lower_match)
            match_posts.append((score, date, md_file['title'], md_files))
        #Si le score est supérieur à la limite demandée, on force le rajout même si ça agrandit la liste
        elif score >= POST_SCORE_LIMIT :
            match_posts.append((score, date, md_file['title'], md_files))
    return match_posts



def posts_list_to_html(posts, sort="date", reverse=True) :
    """ Sort a list of posts by date(default)/alphanum/relevance,
    and return the html code to integrate."""
    if sort == "date" :
        posts = sorted(posts, key=lambda t: t[1], reverse=reverse)
    elif sort == "alphanum" :
        reverse = not reverse #to sort from A to Z.
        posts = sorted(posts, key=lambda t: t[2].lower(), reverse=reverse)
    elif sort == "relevance" :
        posts = sorted(posts, key=lambda t: t[0], reverse=reverse)
    html_code = ""
    for i in posts :
        html_code += HTML_POST_DESCRIPTION.replace(
            '$LINK', i[-1].get_link()).replace(
            '$TITLE', i[-1]['title']).replace(
            '$LOGO', i[-1]['logo']).replace(
            '$DESCRIPTION', i[-1]['description']).replace(
            '$METADATA', POSTS_OPTIONS['metadata_sentence'].format(author=i[-1]['author'], date=i[-1]['date']))
    return html_code



def get_main_title(src) :
    """ Return the page title."""
    #case src is url categorie
    if isinstance(src, str) :
        return COMMON_OPTIONS['categories'][os.path.basename(os.path.dirname(src))]
    #case src is markdown object
    if isinstance(src, MdFile) :
        return src['title']



def get_rss_feed(source) :
    """ Return the rss feed."""
    items = []
    for post in get_latest_posts(source, post_number=FEED_POSTS_NUMBER) :
        md_file = post[3]
        items.append(Item(
            title=md_file['title'],
            link=md_file.get_link(),
            description=md_file['description'],
            creator=md_file['author'],
            guid=Guid(md_file.get_guid()),
            pubDate=md_file.get_date(),
        ))
    feed = Feed(**FEED_INFOS, items=items)
    return feed.rss()
