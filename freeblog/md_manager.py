#!/usr/bin/env python3
# -*- coding : utf-8 -*-

import os
from conf import *
from datetime import datetime
from subprocess import Popen, PIPE


class MdFile :
    """ The markdown start with some information such as author name,
    date of publication, description of article, keywords."""
    def __init__(self, path) :
        #path is : templates/posts/<section>/<article_folder>/<article_filename.md>
        self.path = path
        #__base_link is : /posts/<section>/<article_folder>
        self.__base_link = os.path.dirname(self.path)[9:]+"/"
        self.__metadata = DEFAULT_METADATA.copy()
        self.__load_metadata()

    def __repr__(self) :
        return "MdFile<path:{},author:{},date:{},title:{}>".format(
            self.path, self['author'], self['date'], self['title'])

    def __getitem__(self, key) :
        return self.__metadata[key]

    def __iter__(self) :
        """ Iterate all line of md file."""
        with open(self.path, 'r') as file :
            for i in file.readlines() :
                yield i[:-1]

    def __iter_text(self) :
        """ Iterate only text (without metadata)."""
        lock = True
        for i in self :
            if lock :
                if i.startswith("# ") : lock = False
                else : continue
            yield i

    def __load_metadata(self) :
        """ Set the metadata from file in self."""
        #Try to detect the default logo path
        i = "templates{}{}".format(self.__base_link, POSTS_OPTIONS['default_logo_name'])
        if os.path.exists(i) :
            self.__metadata['logo'] = i[9:]
        #parsing metadata in start of markdown file
        parsing_metadata = False
        in_description = False
        for i in self :
            if i == "---" :
                if parsing_metadata : break
                else :
                    parsing_metadata = True
                    continue
            if i.startswith("# ") :
                if self.__metadata['title'] is None :
                    self.__metadata['title'] = i[2:].strip()
                break
            elif i.startswith("title:") :
                self.__metadata['title'] = i[6:].strip()
            elif i.startswith("author:") :
                self.__metadata['author'] = i[7:].strip()
                in_description = False
            elif i.startswith("date:") :
                self.__metadata['date'] = i[5:].strip()
                in_description = False
            elif i.startswith("description:") :
                self.__metadata['description'] = i[12:].strip()
                in_description = True
            elif i.startswith("logo:") :
                relative_path = i[5:].strip()
                if relative_path.startswith("./") :
                    self.__metadata['logo'] = self.__base_link+relative_path[2:]
                else :
                    self.__metadata['logo'] = self.__base_link+relative_path
            elif i.startswith("keywords:") :
                self.__metadata['keywords'] = [j.strip() for j in i[10:].split(',')]
                in_description = False
            elif in_description :
                self.__metadata['description'] += i

    def get_metadata(self) :
        """ Return the metadatas."""
        return self.__metadata

    def get_link(self) :
        """ Return the ready to use html link."""
        return self.path[9:]

    def get_date(self, unixts=False) :
        """ Return the datetime object or unix timestamp."""
        date = datetime.strptime(self['date'], POSTS_OPTIONS['date_format'])
        if unixts == True :
            return date.timestamp()
        return date

    def get_guid(self) :
        """ Return a uniq id of article, base on title and date."""
        return hash("{}{}".format(self['title'], self['date']))

    def get_text(self) :
        """ Return the text without metadata."""
        text = []
        for i in self.__iter_text() :
            text.append(i)
        return "\n".join(text)

    def get_html(self) :
        """ Return the html text."""
        html = self.__md_proc.render(self.get_text())
        return html



if __name__ == "__main__" :
    path = "templates/posts/actualites/article1.md"
    mdfile = MdFile(path)
    print(mdfile.get_html())
