#!/usr/bin/env python3
# -*- coding : utf-8 -*-


#Nombre d'articles à afficher dans les listes d'article
POSTS_NUMBER = $post_number$

#Nombre d'articles à intégrer dans le flux rss
FEED_POSTS_NUMBER = $feed_posts_number$

#Pour une recherche, si le score de correspondance est supérieur à cette limite,
#on rajoute au résultat, même si on doit dépasser le nombre de posts (POSTS_NUMBER)
POST_SCORE_LIMIT = $post_score_limit$


#OPTION COMMUNE = présent dans toute les pages
COMMON_OPTIONS = {
    #Nom du blog à afficher dans le bandeau supérieur
    "blog_name" : $blog_name$,

    #Page d'accueil (obligatoire)
    #"home" : "/home.html", → your home need to be templates/home.html

    #Liste des catégories possibles dans le bandeau supérieur
    "categories" : {
        #nom de la catégories : nom à afficher
        #doit correpondra à un dossier/fichier : posts/<categorie>/<categorie>.html
        "actualites" : "Actualités",    # correspond à : posts/actualites/actualites.html
        "tutoriels" : "Tutoriels",      #etc.
        "cours" : "Cours",
        "rechercher" : "Rechercher",
        "contacts" : "Contacts",
    },

    #Liste des moyens pour suivre les activités du site (réseau sociaux, etc...)
    #   ("chemin de l'icône correspondante", "url cible") → les images doivent se trouver dans le dossier static/feeds
    "feeds" : $FEEDS$,
    "feed_sentence" : $feed_sentence$,

    #OPTIONS DE LA BARRE DE RECHERCHE, ET ENREGISTREMENT DES DERNIÈRES OPTIONS
    "input_sentence" : $search_sentence$,
    "sort_option" : {
        'sentence' : $search_option_sentence$,   #phrase précédent la liste
        'relevance' : $search_option_relevance$, #défaut
        'date' : $search_option_date$,
        'alphanum' : $search_option_alphanum$,
    },
}

#OPTIONS POUR LES ARTICLES
POSTS_OPTIONS = {
    #Format des dates dans les métadonnées du fichier markdown
    "date_format" : $post_date_format$,

    #Phrase des métadonnées avec clé : author (auteur), date
    "metadata_sentence" : $post_metadata_sentence$,

    #Si le logo n'est pas donné dans les métadonnées mais que le fichier suivant existe à côté, il sera utilisé.
    "default_logo_name" : $default_logo_name$,
}

#INFORMATIONS POUR LE FLUX RSS
FEED_INFOS = {
    #Titre du flux rss
    "title" : $rss_feed_title$,
    #Lien d'accès depuis le flux rss
    "link" : $rss_parent_link$,
    #Description du flux RSS
    "description" : $rss_feed_description$,
    #langue principale du flux
    "language" : $rss_feed_language$,
}

#Description par défaut d'un article
#ATTENTION :
#Selon la date configuré et si le markdown n'intègre pas de date, le document peut finir en fin de liste.
#Si aucune description n'est donnée, les recherches risques de ne jamais donner le document.
DEFAULT_METADATA = {
    "title" : $default_posts_title$,
    "author" : $default_posts_author$,
    "date" : $default_posts_date$,
    "description" : $default_posts_description$,
    "logo" : "/static/images/default_illustration.png", #ne pas toucher celui-ci !
}
