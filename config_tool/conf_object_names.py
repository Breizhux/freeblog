#!/usr/bin/env python3
# -*- coding utf-8 -*-

GTK_CONF_OBJECT_NAMES = [
    "html_background_color", #EEEEEE
    "animation_speed", #0.7s
    "default_font_color", ##FFFFFF
    "default_font_policy", #Cantarell Bold
    "main_container_width", #80vw
    "main_container_margin_top", #80px

    "header_height", #65px
    "header_base_gradient_start", ##5C96E7
    "header_base_gradient_stop", ##640F6F
    "header_base_gradient_orientation", #180deg
    "header_base_radius_top", #0px
    "header_base_radius_bottom", #10px
    "header_anim_gradient_start", ##531752
    "header_anim_gradient_stop", ##3B447D
    "header_anim_gradient_orientation", ##180deg
    "header_anim_radius_top", #0px
    "header_anim_radius_bottom", #10px

    "title_font_color", ##FFFFFF
    "title_font_weight", #600
    "title_shadow_color", ##878787
    "title_minimum_font_size", #70%
    "title_maximum_font_size", #170%

    "categories_width", #6vw
    "categories_height", #4vh
    "categories_border_size", #0px
    "categories_border_color", #1px
    "categories_border_radius", #50px
    "categories_margin", #10px
    "categories_gratient_start", ##893648
    "categories_gratient_middle", ##FCB323
    "categories_gratient_end", ##E53E53
    "categories_gratient_orientation", #120deg
    "categories_font_size", #16px
    "categories_font_color", ##FFFFFF
    "categories_font_shadow_color", ##355a80

    "search_bar_width", #100%
    "search_bar_height", #30px
    "search_bar_radius", #10px
    "search_bar_gradient_start", ##EE883D
    "search_bar_gradient_stop", ##60056E
    "search_bar_gradient_orientation", #90deg
    "search_elem_border_radius", #10px
    "search_elem_font_size", #14px
    "search_elem_border_color_base", ##FFFFFF
    "search_elem_border_color_anim", ##36478D
    "search_box_height", #100%
    "search_box_width", #25em
    "search_label_color", ##FFFFFF
    "search_label_weight", #600
    "search_sort_height", #110%
    "search_sort_width", #12em

    "footer_height", #3em
    "footer_font_color", ##FFFFFF
    "footer_icons_size", #5vh
    "footer_base_gradient_start", ##823A3B
    "footer_base_gradient_stop", ##4C668B
    "footer_base_gradient_orientation", #180deg
    "footer_base_radius_top", #10px
    "footer_base_radius_bottom", #0px
    "footer_anim_gradient_start", ##0C1429
    "footer_anim_gradient_stop", ##4C668B
    "footer_anim_gradient_orientation", #90deg
    "footer_anim_radius_top", #10px
    "footer_anim_radius_bottom", #0px

    "post_base_background_color", ##CCCCCC
    "post_anim_background_color", ##FFFFFF
    "post_border_color", ##000000
    "post_border_size", #1px
    "post_border_radius", #20px
    "post_title_color", ##000000
    "post_title_weight", #600
    "post_title_size", #22px
    "post_title_underline_color", ##000000
    "post_illustration_size", #96px
    "post_metadata_size", #0.8em

    "article_background_color", ##FFFFFF
    "article_border_color", ##CCCCCC
    "article_border_width", #1px
    "article_border_radius", #15px
]


PY_CONF_NAMES = [

    "blog_name", #Mon FreeBlog perso
    "favicon", #ressources/favicon.png
    "blog_logo", #

    "posts_number", #10
    "feed_posts_number", #20
    "post_score_limit", #2000

    "search_sentence", #Rechercher un article
    "search_option_sentence", #Trier par
    "search_option_relevance", #Pertinence
    "search_option_date", #Date
    "search_option_alphanum", #Alphabétique

    "post_date_format", #%d/%m/%Y
    "post_metadata_sentence", #Auteur #{author}, Date #{date}
    "default_logo_name", #illustration.png

    "feed_sentence", #Suivez nous aussi sur ces réseaux :

    "rss_feed_title", #FlusRSS de mon FreeBlog
    "rss_parent_link", #http://127.0.0.1:8080/
    "rss_feed_description", #Ceci est le flux rss de mon FreeBlog.
    "rss_feed_language", #'fr'

    "default_posts_author", #anonyme
    "default_posts_date", #01/01/1970
    "default_posts_description", #Pas de description
    "default_illustration", #ressources/default_illustration.png

] #}


#CATEGORIES = [
#]

#FEEDS = [
#    ("rss_feed.png", "/feed")
#]
