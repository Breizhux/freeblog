#!/usr/bin/env python3
# -*- coding : utf-8 -*-

import os
import gi
import sys
import ast
import shutil
from conf_object_names import *
from subprocess import run, PIPE

gi.require_version("Gtk", "3.0")
gi.require_version("Gdk", "3.0")
from gi.repository import Gtk, Gdk



class PrintColor :
    """ Redefine a print function to allowing color print easily."""
    def __init__(self) :
        self.colors = {
            #reset
            "r" : '\33[0m',
            #text
            "black" : '\33[30m',
            "grey" : '\33[90m',
            "red" : '\33[31m',
            "green" : '\33[32m',
            "yellow" : '\33[33m',
            "blue" : '\33[34m',
            "violet" : '\33[35m',
            "beige" : '\33[36m',
            "white" : '\33[37m',
            #background
            "blackbg" : '\33[40m',
            "greybg" : '\33[100m',
            "redbg" : '\33[41m',
            "greenbg" : '\33[42m',
            "yellowbg" : '\33[43m',
            "bluebg" : '\33[44m',
            "violetbg" : '\33[45m',
            "beigebg" : '\33[46m',
            "whitebg" : '\33[47m',
        }
    def __get_chain(self, *args, sep=' ') :
        chain = sep.join(args)
        for i in self.colors :
            chain = chain.replace(f"<{i}>", self.colors[i])
        return chain
    def print(self, *args, sep=' ', end='\n') :
        chain = self.__get_chain(*args, sep=sep)
        sys.stdout.write(f"{chain}{end}")
        sys.stdout.flush()
    #def print_err(self, *args, sep=' ', end='\n') :
    #    chain = self.__get_chain(*args, sep=sep)
    #    sys.stderr.write(f"{chaine}{end}")
    #    sys.stdout.flush()
print = PrintColor().print



class GtkFileDialog :
    """ Gtk3 have no action save for GtkFileChooserButton.
    Use zenity to use this option."""
    OPTIONS = {
        "title" : "Emplacement du fichier chiffré ou déchiffré.",
        "filename" : os.environ['HOME'],
    }
    def __init__(self, action, label=None) :
        #the parent GtkButton
        self.label = label
        if action == "file" :
            self.command = "zenity --file-selection --title \"{}\" --filename \"{}\""
        elif action == "directory" :
            self.command = "zenity --file-selection --directory --title \"{}\" --filename \"{}\""
        elif action == "save" :
            self.command = "zenity --file-selection --confirm-overwrite --save --title \"{}\" --filename \"{}\""
        self._path = None

    def _set_default_options(self, **kwargs) :
        if self._path != "" :
            self.OPTIONS['filename'] = self._path
        for i in self.OPTIONS :
            if i in kwargs :
                self.OPTIONS[i] = kwargs[i]

    def choose(self, **kwargs) :
        """ Run the zenity command and return True if set a
        valid filename, else return False."""
        self._set_default_options(**kwargs)
        command = self.command.format(self.OPTIONS['title'], self.OPTIONS['filename'])
        process = run(command, stdout=PIPE, shell=True, encoding='utf8')
        if process.stdout == "" :
            self.set_filename(None)
            return False
        else :
            self.set_filename(process.stdout[:-1])
            return True

    def get_filename(self) :
        return self._path

    def set_filename(self, path) :
        self._path = path
        if self.label is None : return None
        if path is None :
            self.label.set_text("(Aucun)")
        else :
            self.label.set_text(os.path.basename(self._path))



class FreeBlogInstaller :
    input_css = "./ressources/css/main.css"
    def __init__(self, work_dir) :
        self.work_dir = work_dir
        self.output_css = f"{self.work_dir}/static/css/main.css"

        ## INTERFACE GTK ##
        self.interface = Gtk.Builder()
        self.interface.add_from_file("./main.glade")
        #the main window
        self.window = self.interface.get_object("window")
        self.window.connect("delete-event", self.quit)
        #affichage
        self.interface.connect_signals(self)
        self.window.show_all()

    def __getitem__(self, name) :
        """ Return the value of Gtk object."""
        key = self.interface.get_object(name)
        if isinstance(key, Gtk.ColorButton) :
            return key.get_rgba().to_string() #get a "rgb(*,*,*)" string
        elif isinstance(key, Gtk.Entry) :
            return key.get_text()
        elif isinstance(key, Gtk.FileChooserButton) :
            return key.get_filename()
        elif isinstance(key, Gtk.Adjustment) :
            return key.get_value()
        elif isinstance(key, Gtk.FontButton) :
            return key.get_font().strip("0123456789 ")
        else :
            print(f"Unknow get fonction for type \"{type(key)}\"")

    def __setitem__(self, name, value) :
        """ Set the value of Gtk object."""
        key = self.interface.get_object(name)
        if isinstance(key, Gtk.ColorButton) :
            color = self.__get_color_from_string(value)
            #color.parse(self.__rgb_to_hexa(value)) #value is of type "rgba(*,*,*,*)", but use "#******" to set color
            key.set_rgba(color)
        elif isinstance(key, Gtk.Entry) :
            key.set_text(value)
        elif isinstance(key, Gtk.FileChooserButton) :
            key.set_filename(value)
        elif isinstance(key, Gtk.Adjustment) :
            key.set_value(value)
        elif isinstance(key, Gtk.FontButton) :
            key.set_font(value)
        else :
            print(f"Unknow set fonction for type \"{type(key)}\"")

    def __get_color_from_string(self, rgba) :
        """ Convert a "rgb(*,*,*)" or "rgba(*,*,*,*) string to the corresponding
        Gdk.RGBA object color. The fucking Gdk.RGBA object __init__ can't set
        rgba attribute from a list and keep it when set in a Gtk.ColorButton 🤯️"""
        #from "rgb(*,*,*)" to 3/4 decimal : r,g,b,a(optionnal)
        values = [int(i) for i in rgba.strip("rgba()").split(",")]
        #convert to hexadecimal color string
        hexa_color = "".join([hex(i).replace('x', '')[-2:] for i in values[:3]])
        #set the Gdk.RGBA color
        color = Gdk.RGBA()
        color.parse(f"#{hexa_color}")
        if len(values) == 4 :
            color.alpha = values[-1] #set alpha channel
        return color

    def quit(self, *args) :
        Gtk.main_quit()

    def apply_properties(self, *args) :
        #write css result
        with open(self.input_css, 'r') as file :
            css_code = file.read()
        for i in GTK_CONF_OBJECT_NAMES :
            css_code = css_code.replace(f"${i}$", str(self[i]))
        with open(self.output_css, 'w') as file :
            file.write(css_code)
        #write python result

    def save_params(self, *args) :
        choose_dialog = GtkFileDialog("save")
        if not choose_dialog.choose() : return None
        gtk_conf = {}
        for i in GTK_CONF_OBJECT_NAMES :
            gtk_conf[i] = self[i]
        with open(choose_dialog.get_filename(), 'w') as file :
            file.write(str(gtk_conf))

    def open_params(self, *args) :
        choose_dialog = GtkFileDialog("file")
        if not choose_dialog.choose() : return None
        with open(choose_dialog.get_filename(), 'r') as file :
            gtk_conf = ast.literal_eval(file.read())
        for i in gtk_conf :
            self[i] = gtk_conf[i]


if __name__ == "__main__" :
    if len(sys.argv) > 1 :
        work_dir = sys.argv[1]
    else :
        print("<red>Error : need to pass the workdir in first argument.<r>")
        print(f"Usage : python3 {sys.argv[0]} [path to freeblog sources]")
        sys.exit(1)
    print("<green>To get an easy preview of settings, start the freeblog developpement server with following commands in another terminal :<r>")
    print(f"    <white>cd \"{work_dir}\"<r>")
    print(f"    <white>python3 app.py<r>")
    print("<green>Then, you can open your favorite browser at this address : <yellow>http://127.0.0.1:8080<r>")
    FreeBlogInstaller(work_dir)
    Gtk.main()
