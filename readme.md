# FreeBlog

## FreeBlog is...

 - **Minimalist and light blog** (full python, Flask).
 - **Very simple to use** (for administrator and redactor) : just send a directory with markdown and attachement to publish an new article.
 - There is **some configuration**.
 - [in progress] The is a very easy to use installer to personnalize your blog ! But you need to write your own html file for main page (home and categories). Categories is section such as "contact", "actuality", etc.

**But once installed, everything will work like clockwork ;)**



## Installation
No package available for the moment, make it from source : 

```bash
#clone this repo
git clone https://gitlab.com/Breizhux/freeblog.git

#Install apt dependency
sudo apt install -y virtualenv

#Install python dependency.
cd freeblog/
virtualenv ./
source bin/activate
pip3 install -r requirements.txt
```



## Test the server
If you want to use this blog in production, you need to use Apache or other solution. The configuration will be done automatically when a debian package is available.

```bash
#run server
cd freeblog/
python3 app.py

#view result at following url :
http://127.0.0.1:8080/
```



## Configure with graphical tool

A graphic tool is here to help you customize your website!


### Installation

```bash
#clone this repo
git clone https://gitlab.com/Breizhux/freeblog.git

#Install apt dependency
sudo apt install -y python3-gi zenity
```


### Run it

```bash
#from where you cloned the repository (last step)
cd "freeblog/config_tool/"

#run gtk interface by specifying where the freeblog app where
python3 config.py "../freeblog"
```



## Write a posts

To create a new post :

- You must create a directory with a certain name : "*article*" for the exemple.

- In this directory, create a file with same name as parent folder with "**.md**" extension : "*article.md*"

- You can add an illustration named as "*illustration.png*" or you can configure the path in metadata of "*article.md*" file with key "logo: path/to/picture.png". In the latter case, the image must be in the "article" folder.

- Your attachment need to be in the folder. You can organize the folder as you want, but all dependancy need to be accessible by url or by put in the folder *article*.

- When article is write, just copy your folder "*article*" in good categories folder (ex: "*/templates/posts/cours/*")


### Metadata

Metadata of markdown need to be write at the start of file with this syntax :
> key: value (without space between key name and ":" !)


|key|description|
|:--|:----------|
|title|The main title of article|
|author|The name of redactor of the article.|
|date|The date of article is write.|
|description|A short description of article.|
|logo|The relative path to the illustration of article.|

NB: I'm abandoning the use of keywords for research. Searches are based on the title of the article as well as the description. The title has 4 times more weight than the description.



## Tree structure

There is equivalent of two project in freeblog :

 - The **freeblog server**.
 - The **graphical configuration tool**.


### 1 - Freeblog

The **entry point** of freeblog server is *freeblog/app.py*.

**Some configuration** is possible in conf.py file. You can edit at the hand (and soon with the graphical configuration tool).

**Categories** corresponding of available button at the top left of website. It need to be configure in *conf.py* file (first step) and have folder in "templates/posts/" with the same name than is write in configuration file. You must also configure the category presentation page yourself, which must have the same name as its folder with the extension ".html".

The **style** is fully write in CSS. The file "static/css/main.css" is use for the **style of website** (header/search bar/footer/resumed article). And the file "static/css/markdown.css" is use for the markdown rendered (title style, cells, etc...).


### 2 - Config tool

The **entry point** of config tool is *config_tool/config.py*.

There is a generic **"main.css"** file with some of variable in replacement of values. This variable is changed by the value set in widget of window configuration.

There is another generic file **conf_ln.py**. In the same principle as generic *main.css*, this file is the configuration file of freeblog.

The **main.glade** was built with the software Glade and describe the configuration window. The different widget have the same name of variable use in generic *main.css*.

The **conf\_object_name.py** contain the list of variable used in *main.css*, *conf_ln.py* and *main.glade*. In this way a simple for loop is used to get value from graphical window and set in *main.css*.

Default and generic file are in "*ressouces*" folder.

You can export and import model. 3 models to see possibilities can found in "*models*" folder.



## Actual result

![capture of freeblog](./actual_website.png)
![capture of configtool](./actual_configtool.png)